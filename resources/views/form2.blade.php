<!DOCTYPE html>
<html>
    <head>
        <title>Form 2</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    </head>
    <body>
        <div style="width: 830px; margin: 0 auto;">
            <h1>Ajax Submission (<a href="{{ URL::to('/') }}">Go to Javascript Form</a>)</h1>
            <br />
            <div id="form_sample_1" style="border: solid thin">
                <h2>Form 1</h2>
                <form action="" method="post" name="submit-form-1">
                    <label>Name: </label><br />
                    <input type="text" name="name"><br />
                    <br />
                    <label>Last Name: </label><br />
                    <input type="text" name="lastname"><br />
                    <br />
                    <button class="submit-btn">Submit Form 1</button>
                </form>
                <br />
            </div>
            <br />
            <div id="form_sample_2" style="border: solid thin">
                <h2>Form 2</h2>
                <form action="" method="post" name="submit-form-2">
                    <label>Name: </label><br />
                    <input type="text" name="name"><br />
                    <br />
                    <label>Last Name: </label><br />
                    <input type="text" name="lastname"><br />
                    <br />
                    <button class="submit-btn">Submit Form 2</button>
                </form>
                <br />
            </div>
            <br />
            <div id="form_sample_3" style="border: solid thin">
                <h2>Form 3</h2>
                <form action="" method="post" name="submit-form-3">
                    <label>Name: </label><br />
                    <input type="text" name="name"><br />
                    <br />
                    <label>Last Name: </label><br />
                    <input type="text" name="lastname"><br />
                    <br />
                    <button class="submit-btn">Submit Form 3</button>
                </form>
                <br />
            </div>
        </div>
        <br />
        <div style="width: 830px; margin: 0 auto;">
            <span style="font-weight: bold; color: red">
                {{ $submitSuccessfully or "" }}
            </span>
        </div>
    </body>
</html>

<script>
    $(document).ready(function() {
        $(".submit-btn").click(function(e) {
            e.preventDefault();

            var formName = $(this).parent().attr("name");

            var formData = {};
            $(this).parent().find("input").each(function() {
                formData[this.name] = this.value;
            });

            $.ajax({
                url: "{{ URL::to("ajax-post") }}",
                type: "POST",
                data: {
                    formDataArr: formData
                },
                success: function(result) {
                    alert("Form ' " + formName + " ' : Insert Successfully.");
                }
            });
        });
    });
</script>
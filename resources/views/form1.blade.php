<!DOCTYPE html>
<html>
    <head>
        <title>Form 1</title>
        <link href="{{ asset("styles/style.css") }}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="main">
            <h1>Create Form using JavaScript (<a href="{{ URL::to('form2') }}">Go to Ajax Submission Form</a>)</h1>
            <div id="form_sample"></div> <!-- Include JS file here -->
            <script src="{{ asset('libraries/form1.js') }}" type="text/javascript"></script>
        </div>
        <br />
        <div style="width: 830px; margin: 0 auto;">
            <span style="font-weight: bold; color: red">
                {{ $submitSuccessfully or "" }}
            </span>

            <br /><br /><hr /><br />
            <span style="font-weight: bold; color: blue">All Rows: </span>
            <br /><br />

            @if(isset($users))
                @foreach($users as $user)
                    First Name: {{ $user->name }}<br />
                    Last Name: {{ $user->lastname }}<br />
                    <br />
                @endforeach
            @endif
        </div>
    </body>
</html>
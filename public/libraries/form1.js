/**
 * Created by Mehdi on 1/22/2016.
 */

// Fetching HTML Elements in Variables by ID.
var x = document.getElementById("form_sample");
var createform = document.createElement('form'); // Create New Element Form
createform.setAttribute("action", ""); // Setting Action Attribute on Form
createform.setAttribute("method", "post"); // Setting Method Attribute on Form
x.appendChild(createform);

var heading = document.createElement('h2'); // Heading of Form
heading.innerHTML = "Javascript Form ";
createform.appendChild(heading);

var line = document.createElement('hr'); // Giving Horizontal Row After Heading
createform.appendChild(line);

var linebreak = document.createElement('br');
createform.appendChild(linebreak);

var namelabel = document.createElement('label'); // Create Label for Name Field
namelabel.innerHTML = "Your Name : "; // Set Field Labels
createform.appendChild(namelabel);

var inputelement = document.createElement('input'); // Create Input Field for Name
inputelement.setAttribute("type", "text");
inputelement.setAttribute("name", "name");
createform.appendChild(inputelement);

var linebreak = document.createElement('br');
createform.appendChild(linebreak);

var lastnamelabel = document.createElement('label'); // Create Label for Last Name Field
lastnamelabel.innerHTML = "Your Last Name : ";
createform.appendChild(lastnamelabel);

var lastnameelement = document.createElement('input'); // Create Input Field for E-mail
lastnameelement.setAttribute("type", "text");
lastnameelement.setAttribute("name", "lastname");
createform.appendChild(lastnameelement);

var lastnamebreak = document.createElement('br');
createform.appendChild(lastnamebreak);

var submitelement = document.createElement('input'); // Append Submit Button
submitelement.setAttribute("type", "submit");
submitelement.setAttribute("name", "submit");
submitelement.setAttribute("value", "Submit");
createform.appendChild(submitelement);
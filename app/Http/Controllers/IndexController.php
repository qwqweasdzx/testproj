<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class IndexController extends Controller
{
    function formGet() {
        $users = User::all();
        return view('form1')->with("users", $users);
    }

    function formPost() {
        $user = new User();
        $user->name = Input::get("name");
        $user->lastname = Input::get("lastname");
        $user->save();

        $users = User::all();

        return view('/form1')
            ->with("submitSuccessfully", "Submitted Successfully.")
            ->with("users", $users);
    }

    function ajaxPost() {
        $formData = Input::get('formDataArr');

        $user = new User();
        $user->name = $formData['name'];
        $user->lastname = $formData['lastname'];
        $user->save();
    }
}

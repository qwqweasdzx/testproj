<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Jenssegers\Mongodb\Model as Moloquent;
class User extends Moloquent
{
    protected $collection = "users";
    public $timestamps = false;
}
